__author__ = 'jinlii'

from django.shortcuts import render_to_response
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import auth

from django.core.mail import send_mail

from showData.models import Line
from showData.forms import ContactForm

def home(request):
  return render_to_response("showData/home.html", {'lines': Line.objects.all()})

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = cd['subject']
            message =  cd['message']
            fromEmailAddress = 'lijingyue@hotmail.com'
            toEmailAddress = cd['email']
            send_mail(subject, message, fromEmailAddress, [toEmailAddress])

            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()

    return render(request, 'showData/contact_form.html', {'form': form})

def thanks(request):
 return render_to_response("showData/thanks.html", {})






