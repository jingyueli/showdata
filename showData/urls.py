from django.conf.urls import include, url
from django.contrib import admin

from showData.views import contact, home, thanks
from showData.accounts.views import *

urlpatterns = [
    # Examples:
    # url(r'^$', 'showData.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home, name='home'),
    url(r'^contact/$', contact, name='contact'),
    url(r'^thanks/$', thanks, name='thanks'),
    url(r"^accounts/login/$",  login),
    url(r"^accounts/auth/$",  auth_view),
    url(r'^accounts/logout/$', logout_view),
    url(r'^accounts/register/$', register_user),
    url(r'^accounts/register_success/$', register_success),
    url(r'^accounts/confirm/(?P<activation_key>\w+)/', register_confirm),
    url(r'^account/loggedin/$', loggedin),
    url(r'^account/invalid/$', invalid),
    url(r'^account/loggedout/$', loggedout),

]
