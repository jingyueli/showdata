__author__ = 'jinli'


from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.template import RequestContext
from django.core.mail import send_mail
from django.utils import timezone

import hashlib
import random
import datetime

from showData.forms import *
from showData.models import *

def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('showData/account/login.html',c)


def loggedin(request):
    return render_to_response("showData/account/loggedin.html", {'full_name': request.user.username})


def invalid(request):
    return render_to_response("showData/account/invalid.html", {})


def loggedout(request):
    return render_to_response("showData/account/loggedout.html", {})


def auth_view(request):

   username = request.POST.get('username', '')
   password = request.POST.get('password', '')
   user = auth.authenticate(username=username, password=password)

   if user is not None and user.is_active:
        # Correct password, and the user is marked "active"
        auth.login(request, user)
        # Redirect to a success page.
        return HttpResponseRedirect("/account/loggedin/")
   else:
        # Show an error page
        return HttpResponseRedirect("/account/invalid/")


def logout_view(request):
    auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/account/loggedout/")


def register_user(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        args['form'] = form
        if form.is_valid():
            form.save()  # save user to database if form is valid

            username = form.cleaned_data['username']
            email = form.cleaned_data['email']

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            salted = (salt + email).encode('utf8')
            activation_key = hashlib.sha1(salted).hexdigest()
            key_expires = datetime.datetime.now() + datetime.timedelta(2)

            #Get user by username
            user = User.objects.get(username=username)

            # Create and save user profile
            new_profile = UserProfile(user=user, activation_key=activation_key,
                key_expires = key_expires)
            new_profile.save()

            # Send email with activation key
            email_subject = 'Account confirmation'
            email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
             48 hours http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)

            send_mail(email_subject, email_body, 'lijingyue@hotmail.com',[email], fail_silently=False)

            return HttpResponseRedirect('/accounts/register_success/')

    else:
        args['form'] = RegistrationForm()

    return render_to_response('showData/account/register_user.html', args, context_instance=RequestContext(request))


def register_success(request):
    return render_to_response("showData/account/register_success.html", {})

def register_confirm(request, activation_key):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        HttpResponseRedirect('/home')

    # check if there is UserProfile which matches the activation key (if not then display 404)
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    #check if the activation key has expired, if it has then render confirm_expired.html
    if user_profile.key_expires < timezone.now():
        return render_to_response('showData/account/registration_confirm_expired.html')

    #if the key hasn't expired save user and set him as active and render some template to confirm activation

    user = user_profile.user
    user.is_active = True
    user.save()

    return render_to_response('showData/account/registration_confirm_success.html')