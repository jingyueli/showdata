__author__ = 'jinli'

from django.db import models
from django.contrib.auth.models import User
import datetime


class Line(models.Model):
    stockName = models.CharField(max_length=255)

    def __str__(self):
        return self.stockName


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=datetime.datetime.now())

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = u'User profiles'